<?php
if (!defined("_ECRIRE_INC_VERSION")) {
	return;
}

function vite(string $entry, $port = "5134"): string {
	defined('VITE_HOST') ||	define('VITE_HOST', "https://localhost:".$port);
	return "\n" .
		jsTag($entry) .
		"\n" .
		jsPreloadImports($entry) .
		"\n" .
		cssTag($entry);
}


function isDev(string $entry): bool {
	static $exists = null;
	if ($exists !== null) {
		return $exists;
	}

	if (
		!empty($_SERVER["SERVER_ADDR"])
		&& in_array($_SERVER["SERVER_ADDR"], ["::1", "127.0.0.1"])
	) {
		$handle = curl_init(VITE_HOST . "/" . $entry);

		curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);

		curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($handle, CURLOPT_NOBODY, true);

		curl_exec($handle);
		$error = curl_errno($handle);
		curl_close($handle);

		return $exists = !$error;

	} else {
		return $exists = false;
	}
}

function jsTag(string $entry): string {
	$url = isDev($entry) ? VITE_HOST . "/" . $entry : assetUrl($entry);

	if (!$url) {
		return "";
	}

	return '<script type="module" crossorigin src="' . $url . '"></script>';
}

function jsPreloadImports(string $entry): string {
	if (isDev($entry)) {
		return "";
	}

	$res = "";
	foreach (importsUrls($entry) as $url) {
		$res .= '<link rel="modulepreload" href="' . $url . '">';
	}

	return $res;
}

function cssTag(string $entry): string {
	if (isDev($entry)) {
		return "";
	}

	$tags = "";
	foreach (cssUrls($entry) as $url) {
		$tags .= '<link rel="stylesheet" href="' . $url . '">';
	}

	return $tags;
}

function getManifest(): array {
	static $urlManifest;

	if ($urlManifest === null) {
		// vite V5
		if (find_in_path("dist/.vite/manifest.json")) {
			$urlManifest = find_in_path("dist/.vite/manifest.json");
		// vite V4
		} else if (find_in_path("dist/manifest.json")) {
			$urlManifest = find_in_path("dist/manifest.json");
		}
	}

	if ($urlManifest) {
		$content = file_get_contents($urlManifest);
		return json_decode($content, true);
	} else {
		die('pas de fichier manifest.json');
	}

}

function assetUrl(string $entry): string {
	$manifest = getManifest();

	return isset($manifest[$entry])
		? find_in_path("dist/" . $manifest[$entry]["file"])
		: "";
}

function importsUrls(string $entry): array {
	$urls = [];
	$manifest = getManifest();

	if (!empty($manifest[$entry]["imports"])) {
		foreach ($manifest[$entry]["imports"] as $imports) {
			$urls[] = find_in_path("dist/" . $manifest[$imports]["file"]);
		}
	}

	return $urls;
}

function cssUrls(string $entry): array {
	$urls = [];
	$manifest = getManifest();

	if (!empty($manifest[$entry]["css"])) {
		foreach ($manifest[$entry]["css"] as $file) {
			$urls[] = find_in_path("dist/" . $file);
		}
	}

	return $urls;
}
