import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import liveReload from "vite-plugin-live-reload";
import path, { dirname } from "path";
import mkcert from 'vite-plugin-mkcert'
import { fileURLToPath } from 'url'

// https://vitejs.dev/config/
export default defineConfig(({ mode }) => {
	const __filename = fileURLToPath(import.meta.url)
	const __dirname = dirname(__filename)
	return {
		// config
		root: "src",
		base: mode === "dev" ? "/" : "/dist/",

		plugins: [
			vue({
				template: {
					transformAssetUrls: {
						base: "plugins/vue_vite",
						includeAbsolute: false,
					},
				},
			}),
			liveReload([__dirname + "/**/*.php", __dirname + "/**/*.html"]),
			// splitVendorChunkPlugin(),
			mkcert()
		],

		build: {
			outDir: "../dist",
			emptyOutDir: true,
			manifest: true,

			rollupOptions: {
				input: path.resolve(__dirname, "src/vue_vite.js"),
			},
		},

		server: {
			strictPort: true,
			port: 5134,
			// https: {
			// 	key: fs.readFileSync("certs/vite.key.pem"),
			// 	cert: fs.readFileSync("certs/vite.crt.pem"),
			// },
			cors: true,
		},

		// required for in-browser template compilation
		// https://vuejs.org/guide/scaling-up/tooling.html#note-on-in-browser-template-compilation
		resolve: {
			alias: {
				vue: "vue/dist/vue.esm-bundler.js",
			},
		},
	}
});
