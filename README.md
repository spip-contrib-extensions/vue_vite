# Vuejs - vite.js

Démonstration du fonctionnement de [Vuesjs](https://vuejs.org/guide/introduction.html) en utilisant [vitejs](https://vitejs.dev/guide/)

> Ce plugin, est pour l'instant, un simple plugin de démonstration, c'est un starter utilisable pour les personnes qui voudraient utilier vuejs.


## Installation
```bash
npm install
#ou
pnpm install
```

## SSL en local
C'est gérer automatiquement par le plugin `vite-plugin-mkcert`
## Fonctionnement
3 commandes sont possibles :
* `npm run dev` lance un server hotreload. Par defaut, c'est sur le port 5134. Pour modifier ce port, il faut modifier le fichier `vite.config.js` et `inc/vite.php`
* `npm run watch` lance une écoute sur une modification d'un fichier. à la différence de `npm run dev`, si une modification est observée, le build va etre refait entierement (donc plus lent et plus couteux en ressources)
* `npm run build` lance le build du projet. une fois le dev réalisé via `npm run dev`, avant de passer en prod, il faut faire le build.

## Squelette
* une fois le plugin installé, il y a une page de demo : `page=demo_vuevite` qui peut comporter 2 parametres (facultatif) : nom et age
* en haut du squelette qui appelle le composant, il faut charger vue :
```html
[(#VAL{vue_vite.js}|vite)]
// ou
[(#VAL{vue_vite.js}|vite{5138})]
// si on veut un autre port d'écoute (dans ce cas, il faut le changer aussi dans vite.config.js)
```
> `vue_vite.js` c'est le nom du fichier js qui lance vuejs. Il suffit d'adapter le nom de ce fichier.


## Quand utiliser Vuejs
> Ce qui suit, n'est que mon avis personnel ;-)

Vuejs n'a pas vocation à **remplacer** les squelettes SPIP. Il peut permettre de creer des composants "hautement" interactifs / dynamiques.
Un autre usage également, c'est la création d'une page / module à partir d'un `json` qui l'on récupère d'une API.

