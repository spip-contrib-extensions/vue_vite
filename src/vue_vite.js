import 'vite/modulepreload-polyfill';
import { createApp } from 'vue';
import VueVite from './components/vueVite.vue';

const components = {
	VueVite
}
const app = createApp({ components });
app.mount('#app');
